#!/usr/bin/env -S python3 -W error
import argparse
import sys
import _io
import io
import base64
from secrets import SystemRandom
from typing import overload, Union


class XorStream:
    """A finite string of bytes repeated infinitely.
    Provides methods to return the keystream or the result of XOR between the
    keystream and a provided byte.
    Take great care in iterating over one of these, since it is infinitely long.
    """

    @overload
    def __init__(self, key: Union[str, bytes, bytearray] = None):
        pass

    @overload
    def __init__(self, size: int = 64):
        pass

    def __init__(self, arg: Union[str, bytes, bytearray, int] = None,
                 key: Union[str, bytes, bytearray] = None, size: int = None):
        """Create a new XorStream object.
         If key is provided as a string, it will be converted to a byte array and used.
         If key is provided as a bytes or bytearray object it will be used as-is.
         If key is NOT provided, a pseudo-random key of size bytes will be generated.
         Only a key OR a size may be provided
         """

        if arg is None:
            # Make sure we only got one kind of init value
            if key is not None and size is not None:
                raise ValueError("Only one of 'key' or 'size' is allowed")
            # See if the parameter was passed by name
            if key is not None:
                arg = key
                key = None
            elif size is not None:
                arg = size
            else:
                arg = 64
        else:
            # arg was provided, so make sure the other two weren't
            if key is not None or size is not None:
                raise ValueError('Only one initialization value is allowed')

        # If we got here, arg is no longer None
        if type(arg) in (bytes, bytearray):
            key = bytes(arg)
        elif type(arg) == str:
            key = bytes(arg, encoding='utf8')
        elif type(arg) == int:
            size = arg
        else:
            raise TypeError("Can't create a key from a " + str(type(arg)))

        self._key = bytes()
        self._curByte = 0
        if key is None:
            rbytes = []
            rand = SystemRandom()
            for i in range(size):
                rbytes.append(rand.getrandbits(8))
            self._key = bytes(rbytes)
        elif type(key) == bytes:
            self._key = bytes(key)

    def __next__(self, block_bytes: int = 1) -> bytes:
        """Support the Python iterator interface. Careful, the stream is infinite."""
        return self.next(block_bytes)

    def next(self, block_bytes: int = 1) -> bytes:
        """Return the next blockBytes bytes of the key stream and advance the current position"""
        cur_val = self[self._curByte: self._curByte + block_bytes]
        self._curByte = (self._curByte + block_bytes) % len(
            self._key)  # Use modular arithmetic to keep _curByte small
        return cur_val

    def xornext(self, original: bytes) -> bytes:
        """XOR an input against the next same-length portion of the keystream and advance the current position"""
        kbytes = self.next(len(original))
        # You'd think you could now just say rval = input ^ kbytes and it would run efficiently.
        # This zip construct seems to be about 2x faster than the bytewise for loop, despite
        # basically being one
        rval = [a ^ b for (a, b) in zip(original, kbytes)]
        return bytes(rval)

    def reset(self):
        """Reset the key stream to the beginning, regardless of current position."""
        self._curByte = 0

    def tofile(self, f: io.FileIO):
        """Save the pseudo-key of a XorStream in file which has been opened for writing.
        If the file object supports binary writes, the data will be written raw.
        If not, the data will be encoded as text using base64
        """

        try:
            # if the file handle supports binary writes, do that
            w = f.write(self._key)
        except TypeError:
            # if not, use base64 encoding
            w = f.write(self.b64string() + "\n")
        f.flush()
        return w

    def b64string(self, encoding: str = 'utf-8') -> str:
        """Return the finite seed for the keystream as a base 64 encoded string"""
        return str(base64.b64encode(self._key), encoding)

    def __getitem__(self, item: Union[int, slice]) -> bytes:
        """Fetch the value of a given byte from the keystream"""
        if type(item) == int:
            return bytes((self._key[item % len(self._key)],))
        elif type(item) == slice:
            sstart = item.start
            sstop = item.stop
            if sstart is None:
                sstart = 0
            if sstop is None:
                # Crap, it's an infinite sequence. I can't slice to the end!
                raise IndexError(
                    "Can't slice to the end of an infinite sequence")

            slicesize = sstop - sstart
            realstart = sstart % len(self._key)
            realend = sstop % len(self._key)

            if slicesize < len(self._key) - realstart:
                # Slice is shorter than what's left of the key after the start point. Just return it
                rval = self._key[realstart:realend]
            else:
                rval = self._key[realstart:]
                sizeleft = slicesize - len(self._key[realstart:])
                sizeleft = sizeleft - len(self._key[:realend])
                infills = sizeleft // len(self._key)
                for i in range(infills):
                    rval += self._key
                rval += self._key[:realend]

            # for i in range(item.start, item.stop):
            #    rval.append(self._key[i % len(self._key)])
            return bytes(rval)
        else:
            raise IndexError('Index was neither an integer nor a slice')

    def __iter__(self):
        return self

    def __str__(self):
        return self._key.decode(encoding='utf-8', errors='backslashreplace')

    def __repr__(self):
        rep = "XorStream object\n"
        rep += "\t    size: " + str(len(self._key)) + "\n"
        rep += "\tposition: " + str(self._curByte) + "\n"
        rep += "\t  base64: " + self.b64string() + "\n"
        rep += "\t   bytes: " + str(list(self._key)) + "\n"
        return rep


class CommandNotReadyError(RuntimeError):
    pass


class UserXorCommand:
    """Class to represent an invocation of the CLI"""

    def __init__(self, argv: Union[str, list] = sys.argv[1:]):
        """Create a command object based on either sys.argv or a passed
        comand string/list
        """

        # If argv was already split, use it as-is. Otherwise split it
        # for use with argparse.parse_args()
        if type(argv) == list:
            self.argv = argv
        elif type(argv) == str:
            self.argv = argv.split()
        else:
            raise TypeError('Need a list or a string to parse arguments')
        self.args = None
        self._key = None

    def parse(self, argv=None):
        """process command line and return the parsed arguments"""

        # Using self.argv as a default argument doesn't work because it's
        # an instance variable and not defined when the class is created
        if argv is None:
            argv = self.argv
        elif type(argv) == str:
            argv = argv.split()

        # Set up the parser with options and arguments
        parser = argparse.ArgumentParser(
            description="""XOR input against a new or existing 'key'. Insecure
                unless the key is true random and as long as the input.""")
        parser.add_argument('--keyin', '-k',
                            type=argparse.FileType(mode='r+b', bufsize=0),
                            help='Path to an existing file which will be used as the pseudo-key for XOR')
        parser.add_argument('--keyout', '-t',
                            type=argparse.FileType(mode='w+b', bufsize=0),
                            help='Path where a randomly generated pseudo-key should be stored')
        parser.add_argument('--random', '-r', nargs='?', default=64, const=64,
                            type=int,
                            help='Generate a random key. Optionally specify size in bytes. Default is 64')
        parser.add_argument('--obs', '-o', default=4096, type=int,
                            help='Output Block Size in bytes. Defaults to 4k')
        parser.add_argument('--verbose', '-v', default=False, const=True,
                            action='store_const',
                            help='Print some status information to STDERR')
        parser.add_argument('--quiet', '-q', default=False, const=True,
                            action='store_const',
                            help='Supress warnings on STDERR')
        parser.add_argument("inputfile", default='-', nargs='?',
                            type=argparse.FileType(mode='r+b', bufsize=0),
                            help='Original file. Uses STDIN if not provided')
        parser.add_argument("outputfile", default='-', nargs='?',
                            type=argparse.FileType(mode='w+b', bufsize=0),
                            help='Output file. Uses STDOUT if not provided')
        self.args = parser.parse_args(argv)

        # argparse ignores the binary mode when opening STDIN and STDOUT.
        # Figure out which class we got and put a reference to the correct binary
        # read/write methods at the top of the args object
        if type(self.args.outputfile) == _io.TextIOWrapper:
            self.args.binWrite = self.args.outputfile.buffer.write
        else:
            self.args.binWrite = self.args.outputfile.write
        if type(self.args.inputfile) == _io.TextIOWrapper:
            # Somehow the STDIN TextIOWrapper doesn't even have a buffer object.
            # Get it from sys.stdin instead
            self.args.binRead = sys.stdin.buffer.read
        else:
            self.args.binRead = self.args.inputfile.read

        # Initialize a XorStream based on the command line arguments
        if self.args.keyin is None:
            # No file was specified, so generate a random one
            self._key = XorStream(size=self.args.random)
            if self.args.keyout is None and not self.args.quiet:
                print(
                    "Warning: using a randomly generated key, but not saving it anywhere! Did you forget --keyout?",
                    file=sys.stderr)
                print(self._key.b64string(), file=sys.stderr)
        else:
            self._key = XorStream(key=self.args.keyin.readall())
            self.args.keyin.close()

    def run(self):
        if self.args is not None:
            # Parsing has happened
            if self.args.verbose:
                print(repr(self._key), file=sys.stderr)
                print(self.args, file=sys.stderr)

            if self.args.keyout is not None:
                self._key.tofile(self.args.keyout)
                self.args.keyout.close()

            while True:
                curr = self.args.binRead(self.args.obs)
                if len(curr) > 0:
                    self.args.binWrite(self._key.xornext(curr))
                else:
                    break
            self.args.inputfile.close()
            self.args.outputfile.close()

        else:
            raise CommandNotReadyError('No arguments have been parsed')


# CLI code using the class and functions above
if __name__ == '__main__':
    cmd = UserXorCommand()
    cmd.parse()
    cmd.run()
