# XOR-cli
A simple command line utility to xor data.

XOR an input file with a fixed-length pseudo-key, which will be re-used
as many times as needed to cover the entire input file. The pseudo-key
may be provided in an existing file, or generated randomly at runtime.

Keep in mind that XOR is **insecure** unless the key is both random and
 as large as the plaintext.

## Usage
```
usage: xor [-h] [--keyin KEYIN] [--keyout KEYOUT] [--random [RANDOM]] [--obs OBS] [--verbose] [--quiet]
           [inputfile] [outputfile]

XOR input against a new or existing 'key'. Insecure unless the key is true random and as long as the input.

positional arguments:
  inputfile             Original file. Uses STDIN if not provided
  outputfile            Output file. Uses STDOUT if not provided

optional arguments:
  -h, --help            show this help message and exit
  --keyin KEYIN, -k KEYIN
                        Path to an existing file which will be used as the pseudo-key for XOR
  --keyout KEYOUT, -t KEYOUT
                        Path where a randomly generated pseudo-key should be stored
  --random [RANDOM], -r [RANDOM]
                        Generate a random key. Optionally specify size in bytes. Default is 64
  --obs OBS, -o OBS     Output Block Size in bytes. Defaults to 4k
  --verbose, -v         Print some status information to STDERR
  --quiet, -q           Supress warnings on STDERR
```